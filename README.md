# OpenML dataset: codrna

https://www.openml.org/d/351

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Andrew V Uzilov","Joshua M Keegan","David H Mathews.  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets) -   
**Please cite**: [AVU06a]
Andrew V Uzilov, Joshua M Keegan, and David H Mathews. 
Detection of non-coding RNAs on the basis of predicted secondary structure formation free energy change. 
BMC Bioinformatics, 7(173), 2006.  

This is the cod-rna dataset, retrieved 2014-11-14 from the libSVM site. Additional to the preprocessing done there (see LibSVM site for details), this dataset was created as follows: 
-join test, train and rest datasets   
-normalize each file columnwise according to the following rules:    
-If a column only contains one value (constant feature), it will set to zero and thus removed by sparsity.    
-If a column contains two values (binary feature), the value occuring more often will be set to zero, the other to one.    
-If a column contains more than two values (multinary/real feature), the column is divided by its std deviation.    

NOTE: please keep in mind that cod-rna has many duplicated data points, within each file &#40;train,test,rest&#41; and also accross these files. these duplicated points have not been removed!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/351) of an [OpenML dataset](https://www.openml.org/d/351). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/351/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/351/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/351/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

